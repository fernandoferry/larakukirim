@extends('layouts.app')
@section('title','Data Barang')
@section('content')
<div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Data Barang</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li class="active">Data Barang</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>
   <div class="row">
        <div class="col-sm-5">
                <div class="panel panel-info">
                        <div class="panel-heading"> Add Data Jenis Barang</div>
                        <div class="panel-wrapper collapse in" aria-expanded="true">
                            <div class="panel-body">
                                <form class="form-horizontal" method="post" action="{{route('barang.store')}}">
                                    @csrf
                                    <div class="form-body">
                                        
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Kode Barang</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" placeholder="Kode Barang" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Jenis Barang</label>
                                            <div class="col-md-9">
                                                <input type="text" name="nama" class="form-control" placeholder="Jenis Barang">
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="form-actions">
                                        <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
                                        <button type="button" class="btn btn-default">Cancel</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="col-sm-7">
                <div class="white-box">
                    <h3 class="box-title">Data Barang</h3>
                    <div class="table-responsive">
                        <table class="table color-table warning-table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Kode Barang</th>
                                    <th>Jenis Barang</th>
                                </tr>
                            </thead>
                            <tbody>
                                @isset($barang)
                                    @foreach ($barang as $item)
                                    <tr>
                                        <td>{{++$i}}</td>
                                        <td>{{$item->id}}</td>
                                        <td>{{$item->nama}}</td>
                                    </tr>
                                    @endforeach
                                @endisset
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
   </div>
@endsection
@section('footer')
<script src="js/dashboard1.js"></script>
<!-- Sparkline chart JavaScript -->
<script src="{{asset('themes/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js')}}"></script>
<script src="{{asset('themes/plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js')}}"></script>
<script src="{{asset('themes/plugins/bower_components/toast-master/js/jquery.toast.js')}}"></script>
<script type="text/javascript">
$(document).ready(function() {
    $.toast({
        heading: 'Welcome back Nadia ID 08321',
        text: 'Segera Cek notifikasi yang anda untuk anda.',
        position: 'top-right',
        loaderBg: '#ff6849',
        icon: 'info',
        hideAfter: 3500,

        stack: 6
    })
});
</script>
<!--Style Switcher -->
<script src="{{asset('themes/plugins/bower_components/styleswitcher/jQuery.style.switcher.js')}}"></script>
@endsection