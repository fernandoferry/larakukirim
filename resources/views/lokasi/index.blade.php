@extends('layouts.app')
@section('title','Data Lokasi')
@section('header')
    <link href="{{asset('themes/plugins/bower_components/datatables/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Data Lokasi</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li class="active">Data Lokasi</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>
   <div class="row">
        <div class="col-sm-5">
                <div class="panel panel-info">
                        <div class="panel-heading"> Daftar Provinsi</div>
                        <div class="panel-wrapper collapse in" aria-expanded="true">
                            <div class="panel-body">
                                    <div class="table-responsive">
                                            <table class="table table-striped datatable">
                                                <thead>
                                                    <tr>
                                                        <th>Kode</th>
                                                        <th>Nama Provinsi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($provinsi as $item)
                                                    <tr>
                                                        <td>{{$item->p_bsni}}</td>
                                                        <td>{{$item->provinsi}}</td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                            </div>
                        </div>
                    </div>
            </div>
        <div class="col-sm-7">
                <div class="panel panel-warning">
                        <div class="panel-heading"> Daftar Kabupaten / Kota</div>
                        <div class="panel-wrapper collapse in" aria-expanded="true">
                            <div class="panel-body">
                                    <div class="table-responsive">
                                            <table class="table table-striped datatable">
                                                <thead>
                                                    <tr>
                                                        <th>Kode</th>
                                                        <th>Kabupaten/Kota</th>
                                                        <th>Provinsi</th>
                                                        <th>Ibu Kota</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($kabkot as $item)
                                                    <tr>
                                                        <td>{{$item->k_bsni}}</td>
                                                        <td>{{$item->hasProvinsi->provinsi}}</td>
                                                        <td>{{$item->kabupaten_kota}}</td>
                                                        <td>{{$item->ibukota}}</td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                            </div>
                        </div>
                    </div>
            </div>
        <div class="col-sm-5">
                <div class="panel panel-success">
                        <div class="panel-heading"> Daftar Kecamatan</div>
                        <div class="panel-wrapper collapse in" aria-expanded="true">
                            <div class="panel-body">
                                    <div class="table-responsive">
                                            <table class="table table-striped datatable">
                                                <thead>
                                                    <tr>
                                                        <th>Kode</th>
                                                        <th>Kecamatan</th>
                                                        <th>Nama Kabupaten/Kota</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($kecamatan as $item)
                                                    <tr>
                                                        <td>{{$item->id}}</td>
                                                        <td>{{$item->kecamatan}}</td>
                                                        <td>{{$item->hasKabkot->kabupaten_kota}}</td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                            </div>
                        </div>
                    </div>
            </div>
        <div class="col-sm-7">
                <div class="panel panel-inverse">
                        <div class="panel-heading"> Daftar Kelurahan</div>
                        <div class="panel-wrapper collapse in" aria-expanded="true">
                            <div class="panel-body">
                                    <div class="table-responsive">
                                            <table class="table table-striped datatable">
                                                <thead>
                                                    <tr>
                                                        <th>Kode</th>
                                                        <th>Kecamatan</th>
                                                        <th>Kelurahan</th>
                                                        <th>Kode Pos</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {{-- @foreach ($kelurahan as $item)
                                                    <tr>
                                                        <td>{{$item->id}}</td>
                                                        <td>{{$item->hasKecamatan->kecamatan}}</td>
                                                        <td>{{$item->kelurahan}}</td>
                                                        <td>{{$item->kd_pos}}</td>
                                                    </tr>
                                                    @endforeach --}}
                                                </tbody>
                                            </table>
                                        </div>
                            </div>
                        </div>
                    </div>
            </div>
   </div>
@endsection
@section('footer')
<script src="js/dashboard1.js"></script>
<!-- Sparkline chart JavaScript -->
<script src="{{asset('themes/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js')}}"></script>
<script src="{{asset('themes/plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js')}}"></script>
<script src="{{asset('themes/plugins/bower_components/toast-master/js/jquery.toast.js')}}"></script>
<script src="{{asset('themes/plugins/bower_components/datatables/jquery.dataTables.min.js')}}"></script>
<!-- start - This is for export functionality only -->
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('.datatable').DataTable();
});
</script>
<!--Style Switcher -->
<script src="{{asset('themes/plugins/bower_components/styleswitcher/jQuery.style.switcher.js')}}"></script>
@endsection