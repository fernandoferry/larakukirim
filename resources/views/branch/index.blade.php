@extends('layouts.app')
@section('title','Data Branch')
@section('header')
<link href="{{asset('themes/plugins/bower_components/bootstrap-select/bootstrap-select.min.css')}}" id="theme" rel="stylesheet">
<link href="{{asset('themes/plugins/bower_components/custom-select/custom-select.css')}}" id="theme" rel="stylesheet">
@endsection
@section('content')
<div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Data Branch</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li class="active">Data Branch</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>
   <div class="row">
        <div class="col-sm-5">
                <div class="panel panel-info">
                        <div class="panel-heading"> {{$dataedit->id ==null ? 'Add' : 'Edit'}} Data Branch</div>
                        <div class="panel-wrapper collapse in" aria-expanded="true">
                            <div class="panel-body">
                                <form class="form-horizontal" method="post" action="{{ $dataedit->id ==null  ?   route('branch.store') : route('branch.update',$dataedit->id)}}">
                                @if($dataedit->id ==null)
                                    @method('PATCH')
                                @endif
                                    @csrf
                                    <div class="form-body">
                                        
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Nama Kantor</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="nama" placeholder="Nama Kantor" value="{{old('nama',$dataedit->nama)}}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Alamat</label>
                                            <div class="col-md-9">
                                                <textarea name="alamat" placeholder="Alamat Lengkap, RT/RW Nomor" class="form-control" >{!! old('nama',$dataedit->alamat)!!}</textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Provinsi</label>
                                            <div class="col-md-9">
                                                <select class="form-control select2" name="id_prov" id="dataProvinsi">
                                                    <option selected>Pilih Provinsi</option>
                                                    @foreach ($provinsi as $item)
                                                    <option value="{{$item->id}}" @if(old('id_prov',$dataedit->id_prov) == $item->id) selected @endif>{{$item->provinsi}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Kota/Kabupaten</label>
                                            <div class="col-md-9">
                                                <select class="form-control select2" name="id_kota" id="dataKota">
                                                        @isset($dataedit->hasKota) 
                                                        <option value="{{old('id_kota',$dataedit->id_kota)}}" selected>{{old('id_prov',$dataedit->hasKota->kabupaten_kota)}}</option>
                                                        @endisset
                                                    </select>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Nomor Telepon</label>
                                            <div class="col-md-9">
                                                <input type="text" name="contact" class="form-control" data-mask="(999) 999-9999" value="{{old('contact',$dataedit->contact)}}">
                                                <span class="font-13 text-muted">(999) 999-9999</span>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="form-actions">
                                        <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
                                        <button type="button" class="btn btn-default">Cancel</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="col-sm-7">
                <div class="white-box">
                    <h3 class="box-title">Daftar Branch</h3>
                    <div class="table-responsive">
                        <table class="table color-table warning-table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nama Kantor</th>
                                    <th>Telepon</th>
                                    <th>Lokasi</th>
                                    <th>action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @isset($data)
                                    @foreach ($data as $item)
                                    <tr>
                                        <td>{{++$i}}</td>
                                        <td>{{$item->nama}}</td>
                                        <td>{{$item->contact}}</td>
                                        <td>{!!"<strong>".$item->hasKota->kabupaten_kota." </strong> - ".$item->hasKota->hasProvinsi->provinsi!!}</td>
                                        <td>
                                            <div class="btn-group">
                                                <a href="{{route('branch.edit',$item->id)}}" class="fcbtn btn btn-info btn-outline btn-1f btn-sm" data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                                                <a href="javascript:void(0);" onclick="removedata('{{route('branch.destroy',$item->id)}}');" class="fcbtn btn btn-danger btn-outline btn-1f btn-sm" data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-times"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                @endisset
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
   </div>
   <form id="delete-form" action=#" method="POST" style="display: none;">
        @method('DELETE')
        @csrf
    </form>
@endsection
@section('footer')
<script src="js/dashboard1.js"></script>
<!-- Sparkline chart JavaScript -->
<script src="{{asset('themes/plugins/bower_components/toast-master/js/jquery.toast.js')}}"></script>
<script src="{{asset('themes/js/mask.js')}}"></script>
<script src="{{asset('themes/plugins/bower_components/custom-select/custom-select.min.js')}}"></script>
<script src="{{asset('themes/plugins/bower_components/bootstrap-select/bootstrap-select.min.js')}}"></script>
<script src="{{asset('themes/plugins/bower_components/multiselect/js/jquery.multi-select.js')}}"></script>
<script type="text/javascript">
$(document).ready(function() {

    
    $.toast({
        heading: 'Welcome back Nadia ID 08321',
        text: 'Segera Cek notifikasi yang anda untuk anda.',
        position: 'top-right',
        loaderBg: '#ff6849',
        icon: 'info',
        hideAfter: 3500,

        stack: 6
    })
    $(".select2").select2();
    function removeOptions(selectbox) { var i; for(i = selectbox.options.length - 1 ; i >= 0 ; i--) { selectbox.remove(i);}
    option = document.createElement( 'option' ); option.text = "Pilih Kota/Kabupaten"; option.disabled = true; selectbox.add( option ); }
    $('#dataProvinsi').change(function(){
        var prov=document.getElementById("dataProvinsi").value;
        $.ajax({
            type : 'get',
            url  : '{{url('/adm/suggest/kota')}}/'+prov,
            success: function(data){
                select = document.getElementById( 'dataKota' );
                removeOptions(select);

                $.each(data, function (index, data) {
                    option = document.createElement( 'option' );
                    option.value = data.id;
                    option.text = data.kabupaten_kota;
                    select.selectedIndex ="0";
                    select.add( option );
                });          
            }
        });
    });

    
    $("#dataKota").focus();
    $('#dataKota').prop('disabled', false);
    document.getElementById('dataKota').selectedIndex="1";
});

function removedata(dataid){
        var result = confirm('Apakan anda yakin akan menghapus data ini?');
        if(result){
            document.getElementById('delete-form').action =dataid;
            document.getElementById('delete-form').submit();
        }
    }
</script>
<!--Style Switcher -->
<script src="{{asset('themes/plugins/bower_components/styleswitcher/jQuery.style.switcher.js')}}"></script>
@endsection