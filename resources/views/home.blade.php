@extends('layouts.app')

@section('content')
<div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Dashboard</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="#">Dashboard</a></li>
                <li class="active">Dashboard</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    
    <div class="row">
        <div class="col-lg-4 col-md-12">
            <div class="white-box">
                <h3 class="box-title"><small class="pull-right m-t-10 text-success"><i class="fa fa-sort-asc"></i> 18% High then last month</small> Site Traffic</h3>
                <div class="stats-row">
                    <div class="stat-item">
                        <h6>Overall Growth</h6> <b>80.40%</b></div>
                    <div class="stat-item">
                        <h6>Montly</h6> <b>15.40%</b></div>
                    <div class="stat-item">
                        <h6>Day</h6> <b>5.50%</b></div>
                </div>
                <div id="sparkline8"></div>
            </div>
        </div>
        <div class="col-lg-4 col-md-12">
            <div class="white-box">
                <h3 class="box-title"><small class="pull-right m-t-10 text-danger"><i class="fa fa-sort-desc"></i> 18% High then last month</small>Admin Traffic</h3>
                <div class="stats-row">
                    <div class="stat-item">
                        <h6>Overall Growth</h6> <b>80.40%</b></div>
                    <div class="stat-item">
                        <h6>Montly</h6> <b>15.40%</b></div>
                    <div class="stat-item">
                        <h6>Day</h6> <b>5.50%</b></div>
                </div>
                <div id="sparkline9"></div>
            </div>
        </div>
        <div class="col-lg-4 col-md-12">
            <div class="white-box">
                <h3 class="box-title"><small class="pull-right m-t-10 text-success"><i class="fa fa-sort-asc"></i> 18% High then last month</small>Transaction Traffic</h3>
                <div class="stats-row">
                    <div class="stat-item">
                        <h6>Overall Growth</h6> <b>80.40%</b></div>
                    <div class="stat-item">
                        <h6>Montly</h6> <b>15.40%</b></div>
                    <div class="stat-item">
                        <h6>Day</h6> <b>5.50%</b></div>
                </div>
                <div id="sparkline10"></div>
            </div>
        </div>
    </div>

     <!-- .row -->
     <div class="row">
        <div class="col-lg-6 col-sm-12 col-xs-12">
            <div class="row">
                <div class="col-lg-6 col-sm-6 col-xs-12">
                    <div class="white-box">
                        <h3 class="box-title">Pelanggan Baru</h3>
                        <ul class="list-inline two-part">
                            <li><i class="icon-people text-info"></i></li>
                            <li class="text-right"><span class="counter">23</span></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-6 col-xs-12">
                    <div class="white-box">
                        <h3 class="box-title">Barang di Gudang</h3>
                        <ul class="list-inline two-part">
                            <li><i class="icon-folder text-purple"></i></li>
                            <li class="text-right"><span class="counter">169</span></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-6 col-xs-12">
                    <div class="white-box">
                        <h3 class="box-title">Barang Telah Sampai</h3>
                        <ul class="list-inline two-part">
                            <li><i class="icon-folder-alt text-danger"></i></li>
                            <li class="text-right"><span class="counter">3211</span></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-6 col-xs-12">
                    <div class="white-box">
                        <h3 class="box-title">Invoice Bulan Ini</h3>
                        <ul class="list-inline two-part">
                            <li><i class="ti-wallet text-success"></i></li>
                            <li class="text-right"><span class="counter">4032</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12 col-xs-12">
            <div class="news-slide m-b-15">
                <div class="vcarousel slide">
                    <!-- Carousel items -->
                    <div class="carousel-inner">
                        <div class="active item">
                            <div class="overlaybg"><img src="plugins/images/property/prop4.jpeg" /></div>
                            <div class="news-content"><span class="label label-danger label-rounded">News</span>
                                <h2>Pengiriman Ke Daerah Sumatera : Expedisi terjadi delay di Pelabuhan mengakibatkan keterlambatan armada sampai tujuan...</h2> <a href="#">Read More</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->


    <!--row -->
    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 pull-right">
            <div class="white-box">
                <h3 class="box-title">Grafik Data</h3>
                <ul class="list-inline text-right">
                    <li>
                        <h5><i class="fa fa-circle m-r-5" style="color: #00bfc7;"></i>Pengiriman</h5>
                    </li>
                    <li>
                        <h5><i class="fa fa-circle m-r-5" style="color: #fdc006;"></i>Kedatangan</h5>
                    </li>
                </ul>
                <div id="morris-area-chart2" style="height: 370px;"></div>
            </div>
        </div>
    </div>
    <!-- row -->
@endsection
@section('footer')
<script src="js/dashboard1.js"></script>
<!-- Sparkline chart JavaScript -->
<script src="{{asset('themes/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js')}}"></script>
<script src="{{asset('themes/plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js')}}"></script>
<script src="{{asset('themes/plugins/bower_components/toast-master/js/jquery.toast.js')}}"></script>
<script type="text/javascript">
$(document).ready(function() {
    $.toast({
        heading: 'Welcome back Nadia ID 08321',
        text: 'Segera Cek notifikasi yang anda untuk anda.',
        position: 'top-right',
        loaderBg: '#ff6849',
        icon: 'info',
        hideAfter: 3500,

        stack: 6
    })
});
</script>
<!--Style Switcher -->
<script src="{{asset('themes/plugins/bower_components/styleswitcher/jQuery.style.switcher.js')}}"></script>
@endsection