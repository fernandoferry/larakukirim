<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();
Route::group(['middleware' => ['auth']], function () {
    Route::prefix('adm')->group(function () {
        Route::get('/home', 'HomeController@index')->name('home');
        Route::resource('barang','BarangController');
        Route::resource('branch','BranchController');
        Route::resource('employee','EmployeeController');
        Route::get('master/lokasi','CMasterData@lokasi')->name('master.lokasi');
        Route::get('suggest/kota/{idprov}','CMasterData@suggestKota')->name('suggest.kota');
    });
});
