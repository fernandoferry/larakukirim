<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kabkot extends Model
{
    protected $table = 'kabkot';
    protected $primaryKey = 'id';
    protected $fillable = ['provinsi_id','kabupaten_kota','ibukota','k_bsni'];
    public $timestamps = false;

    public function hasProvinsi()
    {
        return $this->belongsTo(Provinsi::class,'provinsi_id'); 
    }

    public function hasKecamatan()
    {
        return $this->hasMany(Kecamatan::class,'kabkot_id');
    }
}
