<?php

namespace App\Http\Controllers;

use App\Provinsi;
use App\Kabkot;
use App\Branch;
use Illuminate\Http\Request;

class BranchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Branch::orderBy('id','desc')->get();
        $provinsi   = Provinsi::all();
        $kabkot     = Kabkot::all();
        return view('branch.index',compact('data','provinsi','kabkot'))->with(['dataedit' => new Branch()])->with('i');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Branch();
        $data->nama = $request->nama;
        $data->id_kota = $request->id_kota;
        $data->id_prov = $request->id_prov;
        $data->alamat = $request->alamat;
        $data->contact = $request->contact;
        $data->save();

        return redirect()->back()->with('success','Berhasil menambahkan data branch');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function show(Branch $branch)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Branch::orderBy('id','desc')->get();
        $provinsi   = Provinsi::all();
        $kabkot     = Kabkot::all();
        $dataedit = Branch::findOrFail($id);
        return view('branch.index',compact('data','provinsi','kabkot','dataedit'))->with('i');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Branch::findOrFail($id);
        $data->nama = $request->nama;
        if(isset($request->id_kota)){
            $data->id_kota = $request->id_kota;
        }
        $data->id_prov = $request->id_prov;
        $data->alamat = $request->alamat;
        $data->contact = $request->contact;
        $data->save();
        return redirect()->route('branch.index')->with('success','Berhasil memperbaharui data branch');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Branch::findOrFail($id)->delete();
        return redirect()->route('branch.index')->with('success','Berhasil menghapus data branch');
    }
}
