<?php

namespace App\Http\Controllers;
use App\Provinsi;
use App\Kabkot;
use App\Kecamatan;
use App\Kelurahan;
use Illuminate\Http\Request;

class CMasterData extends Controller
{
    public function lokasi()
    {
        $provinsi   = Provinsi::all();
        $kabkot     = Kabkot::all();
        $kecamatan = Kecamatan::limit(200)->get();
        $kelurahan     = Kelurahan::limit(200)->get();
        return view('lokasi.index',compact('provinsi','kabkot','kecamatan','kelurahan'));
    }

    public function suggestKota($idprov)
    {
        $kabkot = Kabkot::select('kabkot.id','kabkot.kabupaten_kota')->where('provinsi_id',$idprov)->get();

        return response($kabkot);
    }
}
