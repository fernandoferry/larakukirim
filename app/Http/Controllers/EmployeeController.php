<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\Branch;
use App\Employee;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Employee::orderBy('user_id','DESC')->get();
        $branch = Branch::orderBy('id','desc')->get();
        return view('employee.index',compact('data','branch'))->with(['dataedit' => new Employee()])->with('i');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request, [
                'name'=>'required',
                'email'=>'required|email|unique:users',
                'password'=>'required|min:6|confirmed'
            ]
        );

        $user = User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
        ]);

        $employee = new Employee();
        $employee->user_id = $user->id;
        $employee->nama = $request->name;
        $employee->alamat = $request->alamat;
        $employee->jabatan = $request->jabatan;
        $employee->id_branch = $request->id_branch;
        $employee->contact = $request->contact;

        $employee->save();
        return redirect()->back()->with('success','Berhasil menambahkan data karyawan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dataedit=Employee::where('user_id',$id)->first();
        $branch = Branch::orderBy('id','desc')->get();
        $data = Employee::orderBy('user_id','DESC')->get();

        return view('employee.index',compact('data','dataedit','branch'))->with('i');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        //
    }
}
