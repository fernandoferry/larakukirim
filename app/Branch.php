<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    // protected $table = 'barangs';
    // protected $primaryKey = 'id';
   	protected $fillable = ['nama','id_kota','id_prov','alamat','contact'];
    // public $timestamps = false;

    public function hasKota()
    {
        return $this->hasOne(Kabkot::class,'id','id_kota');
    }
}
