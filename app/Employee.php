<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $table = 'employees';
    // protected $primaryKey = 'id';
    protected $fillable = ['user_id','nama','alamat','id_branch','contact'];
    // public $timestamps = false;

    public function hasUser()
    {
        return $this->hasOne(User::class,'id','user_id');
    }

    public function hasBranch()
    {
        return $this->hasOne(Branch::class,'id','id_branch');
    }
}
