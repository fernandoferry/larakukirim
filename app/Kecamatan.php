<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kecamatan extends Model
{
    protected $table = 'kecamatan';
    protected $primaryKey = 'id';
    protected $fillable = ['kabkot_id','kecamatan'];
    public $timestamps = false;

    public function hasKabkot()
    {
        return $this->belongsTo(Kabkot::class,'kabkot_id'); 
    }

    public function hasKelurahan()
    {
        return $this->hasMany(Kelurahan::class,'kecamatan_id');
    }
}
