<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kelurahan extends Model
{
    protected $table = 'kelurahan';
    protected $primaryKey = 'id';
    protected $fillable = ['kecamatan_id','kelurahan','kd_pos'];
    public $timestamps = false;

    public function hasKecamatan()
    {
        return $this->belongsTo(Kecamatan::class,'kecamatan_id'); 
    }
}
