<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provinsi extends Model
{
    protected $table = 'provinsi';
    protected $primaryKey = 'id';
    protected $fillable = ['provinsi','p-bsni'];
    public $timestamps = false;

    public function hasKota()
    {
        return $this->hasMany(Kabkot::class,'provinsi_id');
    }
}
